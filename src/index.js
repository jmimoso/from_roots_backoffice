import 'babel-polyfill';
import './styles/styles.scss';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './controllers/App';


ReactDOM.render(<App />, document.getElementById('react-root'));